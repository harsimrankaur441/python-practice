import urllib.request, urllib.parse, urllib.error
import ssl
import json

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE


url = input('Enter - ')
json_data = urllib.request.urlopen(url, context=ctx).read().decode() #As urllib takes cares of encoding itself i.e encode data from charcter string to bytes string .Decode will change the bytes string to character string

data = json.loads(json_data)
#print(data["comments"])
l = len(data["comments"])
#print(l)
sum = 0
for x in range(0,l):
    i = int(data["comments"][x]["count"])
    sum = sum + i

print(sum)
