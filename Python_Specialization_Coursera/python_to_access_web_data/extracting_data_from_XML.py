import urllib.request, urllib.parse, urllib.error
import xml.etree.ElementTree as ET
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE


url = input('Enter - ')
xml = urllib.request.urlopen(url, context=ctx).read().decode() #As urllib takes cares of encoding itself i.e encode data from charcter string to bytes string .Decode will change the bytes string to character string


tree = ET.fromstring(xml) #this will create tree of xml tags
mylist = tree.findall('comments/comment') #list of trees with parent nodes as comment and child as count

counts = []
for x in mylist:
    counts.append(int(x.find('count').text))

print(sum(counts))


''' Input data ---
<commentinfo>
    <comments>
        <comment>
        <name>Alphonsina</name>
        <count>100</count>
        </comment>
        <comment>..</comment>
    </comments>
</commentinfo>
'''
